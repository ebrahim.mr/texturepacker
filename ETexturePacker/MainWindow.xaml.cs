﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ETexturePacker
{
    public partial class MainWindow : Window
    {
        private enum TextureType { Specular, Roughness, Height, Ao }
        private readonly string[] _imageExtensions = { ".JPG", ".JPEG", ".TGA", ".PNG" };

        private TextureType _type;
        private readonly OpenFileDialog _dialog;
        private string _specPath;
        private string _roughnessPath;
        private string _heightPath;
        private string _aoPath;
        private string _fileName;
        private string _outputPath;
        private BackgroundWorker _worker;
        private Core.PackingData _packingData;
        private bool _error;
        private bool _customFileNameIsSelected;
        private About _about;

        public MainWindow() {
            InitializeComponent();

            _dialog = new OpenFileDialog {
                Filter = "Image Files|*.jpg;*.jpeg;*.png;*.tga;",
                Title = "Please select the map for this channel"
            };
            _dialog.FileOk += Dialog_FileOk;
            _dialog.Multiselect = false;
            _dialog.ShowReadOnly = true;

            BtnChangeSpec.Visibility = Visibility.Collapsed;
            BtnChangeRough.Visibility = Visibility.Collapsed;
            BtnChangeHeight.Visibility = Visibility.Collapsed;
            BtnChangeAO.Visibility = Visibility.Collapsed;

            BtnRemoveSpec.Visibility = Visibility.Collapsed;
            BtnRemoveRough.Visibility = Visibility.Collapsed;
            BtnRemoveHeight.Visibility = Visibility.Collapsed;
            BtnRemoveAO.Visibility = Visibility.Collapsed;

            _worker = new BackgroundWorker {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            _worker.DoWork += Worker_DoWork;
            _worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            _worker.ProgressChanged += Worker_ProgressChanged;

            ProgressBar.Value = 0;

            _packingData = new Core.PackingData {
                IsPNG = true,
                UseNewFormat = true
            };

            _outputPath = $"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}\\PackedTexture.png";
            OutputPath.Text = _outputPath;

            PreviewKeyDown += MainWindow_PreviewKeyDown;
            _customFileNameIsSelected = false;
            GridButton.IsEnabled = true;
        }

        #region UI Stuff
        private void MainWindow_PreviewKeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Escape)
                Close();
            else if (e.Key == Key.M)
                WindowState = WindowState.Minimized;
        }
        private void Start_Click(object sender, RoutedEventArgs e) {
            GridButton.IsEnabled = false;
            LogOutput("Packing is in progress");
            _worker.RunWorkerAsync();
        }
        void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            Dispatcher.Invoke(() => {
                ProgressBar.Value = e.ProgressPercentage;
            });
        }
        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (!_error) {
                LogOutput("Textures packed successfully.");
                _customFileNameIsSelected = false;
            }
            GridButton.IsEnabled = true;
        }

        //Button Clicks
        private void SpecularButton_Click(object sender, RoutedEventArgs e) {
            _type = TextureType.Specular;
            _dialog.ShowDialog(this);
        }
        private void BtnRough_Click(object sender, RoutedEventArgs e) {
            _type = TextureType.Roughness;
            _dialog.ShowDialog(this);
        }
        private void BtnHeight_Click(object sender, RoutedEventArgs e) {
            _type = TextureType.Height;
            _dialog.ShowDialog(this);
        }
        private void BtnAO_Click(object sender, RoutedEventArgs e) {
            _type = TextureType.Ao;
            _dialog.ShowDialog(this);
        }

        //Remove Button Clicks
        private void BtnRemoveSpec_Click(object sender, RoutedEventArgs e) {
            RemoveTexture_Clicked(TextureType.Specular);
        }
        private void BtnRemoveRough_Click(object sender, RoutedEventArgs e) {
            RemoveTexture_Clicked(TextureType.Roughness);
        }
        private void BtnRemoveHeight_Click(object sender, RoutedEventArgs e) {
            RemoveTexture_Clicked(TextureType.Height);
        }
        private void BtnRemoveAO_Click(object sender, RoutedEventArgs e) {
            RemoveTexture_Clicked(TextureType.Ao);
        }

        //Change Button Clicks
        private void BtnChangeSpec_Click(object sender, RoutedEventArgs e) {
            _type = TextureType.Specular;
            _dialog.ShowDialog(this);
        }
        private void BtnChangeRough_Click(object sender, RoutedEventArgs e) {
            _type = TextureType.Roughness;
            _dialog.ShowDialog(this);
        }
        private void BtnChangeHeight_Click(object sender, RoutedEventArgs e) {
            _type = TextureType.Height;
            _dialog.ShowDialog(this);
        }
        private void BtnChangeAO_Click(object sender, RoutedEventArgs e) {
            _type = TextureType.Ao;
            _dialog.ShowDialog(this);
        }

        //DragDrop Events
        private void GridSpec_Drop(object sender, DragEventArgs e) {
            DragDrop_Handler(TextureType.Specular, e);
        }
        private void GridAO_Drop(object sender, DragEventArgs e) {
            DragDrop_Handler(TextureType.Ao, e);
        }
        private void GridHeight_Drop(object sender, DragEventArgs e) {
            DragDrop_Handler(TextureType.Height, e);
        }
        private void GridRough_Drop(object sender, DragEventArgs e) {
            DragDrop_Handler(TextureType.Roughness, e);
        }

        private void NewFile_Click(object sender, RoutedEventArgs e) {
            RemoveTexture_Clicked(TextureType.Specular);
            RemoveTexture_Clicked(TextureType.Height);
            RemoveTexture_Clicked(TextureType.Roughness);
            RemoveTexture_Clicked(TextureType.Ao);

            ProgressBar.Value = 0;

            _packingData.Reset();

            _outputPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\PackedTexture.png";
            OutputPath.Text = _outputPath;

            LogOutput("Ready");

            _customFileNameIsSelected = false;
        }
        private void Close_Click(object sender, RoutedEventArgs e) {
            Close();
        }
        private void About_Click(object sender, RoutedEventArgs e) {
            _about = new About();
            _about.Show();
        }
        private void OutputFormat_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if ((e.AddedItems[0] as ComboBoxItem)?.Content == null) {
                return;
            }

            _outputPath = _outputPath.Remove(_outputPath.Length - 4, 4);

            if ((e.AddedItems[0] as ComboBoxItem)?.Content as string == "PNG") {
                _packingData.IsPNG = true;
                _outputPath += ".png";
            } else {
                _packingData.IsPNG = false;
                _outputPath += ".tga";
            }

            OutputPath.Text = _outputPath;
        }
        private void RLE_Click(object sender, RoutedEventArgs e) {
            if (RLE.IsChecked != null) _packingData.UseRLE = (bool)RLE.IsChecked;
        }
        private void TGANewFormat2_Click(object sender, RoutedEventArgs e) {
            if (TGANewFormat2.IsChecked != null) _packingData.UseRLE = (bool)TGANewFormat2.IsChecked;
        }
        private void ColorMap2BytesEntry_Click(object sender, RoutedEventArgs e) {
            if (ColorMap2BytesEntry.IsChecked != null) _packingData.UseRLE = (bool)ColorMap2BytesEntry.IsChecked;
        }
        #endregion
        //Operational Sections
        private void Dialog_FileOk(object sender, CancelEventArgs e) {
            var path = ((OpenFileDialog)sender).FileName;
            var imgBrush = new ImageBrush {
                Stretch = Stretch.UniformToFill,
                ImageSource = new BitmapImage(new Uri(path, UriKind.Absolute))
            };

            if (!_customFileNameIsSelected) {
                _fileName = Path.GetFileNameWithoutExtension(path);

                var index = _fileName.IndexOf("_", StringComparison.Ordinal);
                if (index > 0)
                    _fileName = _fileName.Substring(0, index);

                _outputPath = $"{Path.GetDirectoryName(path)}\\{_fileName}_SRHA{(_packingData.IsPNG ? ".png" : ".tga")}";
                OutputPath.Text = _outputPath;
            }

            switch (_type) {
                case TextureType.Specular:
                    _specPath = path;
                    GridSpec.Background = imgBrush;
                    BtnSpec.Visibility = Visibility.Collapsed;
                    BtnChangeSpec.Visibility = Visibility.Visible;
                    BtnRemoveSpec.Visibility = Visibility.Visible;
                    break;
                case TextureType.Roughness:
                    _roughnessPath = path;
                    GridRough.Background = imgBrush;
                    BtnRough.Visibility = Visibility.Collapsed;
                    BtnChangeRough.Visibility = Visibility.Visible;
                    BtnRemoveRough.Visibility = Visibility.Visible;
                    break;
                case TextureType.Height:
                    _heightPath = path;
                    GridHeight.Background = imgBrush;
                    BtnHeight.Visibility = Visibility.Collapsed;
                    BtnChangeHeight.Visibility = Visibility.Visible;
                    BtnRemoveHeight.Visibility = Visibility.Visible;
                    break;
                case TextureType.Ao:
                    _aoPath = path;
                    GridAO.Background = imgBrush;
                    BtnAO.Visibility = Visibility.Collapsed;
                    BtnChangeAO.Visibility = Visibility.Visible;
                    BtnRemoveAO.Visibility = Visibility.Visible;
                    break;
                default:
                    break;
            }

        }
        private void RemoveTexture_Clicked(TextureType textureType) {
            switch (textureType) {
                case TextureType.Specular:
                    GridSpec.Background = null;
                    BtnSpec.Visibility = Visibility.Visible;
                    BtnChangeSpec.Visibility = Visibility.Collapsed;
                    BtnRemoveSpec.Visibility = Visibility.Collapsed;
                    _specPath = string.Empty;
                    break;
                case TextureType.Roughness:
                    GridRough.Background = null;
                    BtnRough.Visibility = Visibility.Visible;
                    BtnChangeRough.Visibility = Visibility.Collapsed;
                    BtnRemoveRough.Visibility = Visibility.Collapsed;
                    _roughnessPath = string.Empty;
                    break;
                case TextureType.Height:
                    GridHeight.Background = null;
                    BtnHeight.Visibility = Visibility.Visible;
                    BtnChangeHeight.Visibility = Visibility.Collapsed;
                    BtnRemoveHeight.Visibility = Visibility.Collapsed;
                    _heightPath = string.Empty;
                    break;
                case TextureType.Ao:
                    GridAO.Background = null;
                    BtnAO.Visibility = Visibility.Visible;
                    BtnChangeAO.Visibility = Visibility.Collapsed;
                    BtnRemoveAO.Visibility = Visibility.Collapsed;
                    _aoPath = string.Empty;
                    break;
                default:
                    break;
            }
        }
        private void DragDrop_Handler(TextureType textureType, DragEventArgs e) {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) {

                var files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (files != null && files.Length >= 1) {
                    var ext = Path.GetExtension(files[0])?.ToUpperInvariant();
                    var isImage = false;

                    foreach (var iExt in _imageExtensions) {
                        if (iExt == ext) {
                            isImage = true;
                            break;
                        }
                    }

                    if (!isImage)
                        return;

                    if (!_customFileNameIsSelected) {
                        _fileName = Path.GetFileNameWithoutExtension(files[0]);

                        var index = _fileName.LastIndexOf("_", StringComparison.Ordinal);
                        if (index > 0)
                            _fileName = _fileName.Substring(0, index);

                        _outputPath = $"{Path.GetDirectoryName(files[0])}\\{_fileName}_SRHA{(_packingData.IsPNG ? ".png" : ".tga")}";
                        OutputPath.Text = _outputPath;
                    }

                    var imgBrush = new ImageBrush {
                        Stretch = Stretch.UniformToFill,
                        ImageSource = new BitmapImage(new Uri(files[0], UriKind.Absolute))
                    };

                    switch (textureType) {
                        case TextureType.Specular:
                            _specPath = files[0];
                            BtnSpec.Visibility = Visibility.Collapsed;
                            BtnChangeSpec.Visibility = Visibility.Visible;
                            BtnRemoveSpec.Visibility = Visibility.Visible;
                            GridSpec.Background = imgBrush;
                            GridSpec.ToolTip = Path.GetFileName(files[0]);
                            break;
                        case TextureType.Roughness:
                            _roughnessPath = files[0];
                            BtnRough.Visibility = Visibility.Collapsed;
                            BtnChangeRough.Visibility = Visibility.Visible;
                            BtnRemoveRough.Visibility = Visibility.Visible;
                            GridRough.Background = imgBrush;
                            GridRough.ToolTip = Path.GetFileName(files[0]);
                            break;
                        case TextureType.Height:
                            _heightPath = files[0];
                            BtnHeight.Visibility = Visibility.Collapsed;
                            BtnChangeHeight.Visibility = Visibility.Visible;
                            BtnRemoveHeight.Visibility = Visibility.Visible;
                            GridHeight.Background = imgBrush;
                            GridHeight.ToolTip = Path.GetFileName(files[0]);
                            break;
                        case TextureType.Ao:
                            _aoPath = files[0];
                            BtnAO.Visibility = Visibility.Collapsed;
                            BtnChangeAO.Visibility = Visibility.Visible;
                            BtnRemoveAO.Visibility = Visibility.Visible;
                            GridAO.Background = imgBrush;
                            GridAO.ToolTip = Path.GetFileName(files[0]);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        private void ChangeLocation_Click(object sender, RoutedEventArgs e) {
            // Configure save file dialog box
            var saveDialog = new SaveFileDialog {
                FileName = "ETexturePacker", // Default file name
                DefaultExt = ".png", // Default file extension
                Filter = "Image Files|*.jpg;*.jpeg;*.png;*.tga;" // Filter files by extension
            };

            if ((bool)saveDialog.ShowDialog()) {
                // Save document
                _outputPath = saveDialog.FileName;
                OutputPath.Text = _outputPath;
                _customFileNameIsSelected = true;
            }
        }
        private void Worker_DoWork(object sender, DoWorkEventArgs e) {
            try {
                _packingData.RMapPath = _specPath;
                _packingData.GMapPath = _roughnessPath;
                _packingData.BMapPath = _heightPath;
                _packingData.AMapPath = _aoPath;

                _packingData.OutputPath = _outputPath;

                Core.Pack(_packingData, ref _worker);

                _error = false;
            } catch (Exception ex) {
                _error = true;
                LogOutput(ex.Message);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            GridButton.IsEnabled = true;
        }
        private void LogOutput(string message) {
            Dispatcher.Invoke(() => {
                Log.Content = message;
            });
        }
        private void OutputPath_TextChanged(object sender, TextChangedEventArgs e) {
            _customFileNameIsSelected = true;
        }
    }
}