﻿using System.Windows;
using System.Windows.Documents;

namespace ETexturePacker
{
    public partial class About : Window
    {
        public About() {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            Close();
        }

        private void Hyperlink_Click(object Sender, RoutedEventArgs E) {
            var url = (Sender as Hyperlink)?.NavigateUri;
            if (url != null)
                System.Diagnostics.Process.Start(url.OriginalString);
        }
    }
}
